package com.indosat.controller;

import com.indosat.exception.CustomException;
import com.indosat.model.ServiceDataResponse;
import com.indosat.model.SyncEligibilityResponse;
import com.indosat.repository.EligibilitySyncStatusRepo;
import com.indosat.service.CheckEligibilityService;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;
import org.json.XML;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

@RestController
@Slf4j
@RequestMapping
public class SyncEligibilityRest {

    @Autowired
    CheckEligibilityService service;

    @Value("${payload.xml.header}")
    private String payloadXmlHeader;

    @Value("${payload.xml.body}")
    private String payloadXmlBody;

    @Value("${payload.xml.request}")
    private String payloadXmlRequest;

    @Value("${payload.xml.transId}")
    private String PayloadXmlTransId;

    @Value("${payload.xml.msisdn}")
    private String PayloadXmlMsisdn;

    @Value("${payload.xml.serviceName}")
    private String PayloadXmlServiceName;

    @Value("${thread.sleep.time}")
    private int sleepTime;

    @Value("${thread.max.counter}")
    private int counter;

    @Value("${response.xml.status}")
    private String responseStatus;

    @Value("${response.xml.header}")
    private String responseHeader;

    @Autowired
    EligibilitySyncStatusRepo repo;

    @GetMapping(value = "/helloWorld")
    public ResponseEntity<String> helloWorld(){
        return new ResponseEntity<>("Hello world", HttpStatus.OK);
    }

    @PostMapping(value = "/insertData")
    public ResponseEntity insertData (@RequestBody String param){
        boolean result = false;
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            JSONObject jsonObject = new JSONObject(param);
            String transId = jsonObject.getString("transid");
            result = service.insertData(jsonObject, transId);
            map.put("status", 0);
            map.put("message", "successfully input data");
            return new ResponseEntity(map, HttpStatus.OK);
        }catch (Exception e){
            log.error(e.getMessage());
            e.printStackTrace();
            map.put("status", 99);
            map.put("message", e.getMessage());
            return new ResponseEntity<>(map, HttpStatus.BAD_REQUEST);
        }
    }


    @PostMapping(value = "/checkEligibility", consumes = {"*/*"}, produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity checkEligibility(@RequestBody String data) throws CustomException, Exception {
        try{
            // convert plain text to xml doc
            Document doc = convertStringToXml(data);
            String xml = convertDocumentToString(doc);

            JSONObject jsonObject = XML.toJSONObject(xml);
            JSONObject jsonObject1 = ((JSONObject) jsonObject.get(payloadXmlRequest));
            JSONObject jsonObject2 = ((JSONObject) jsonObject.get(payloadXmlRequest))
                    .getJSONObject("con:ATTRIBUTES").getJSONObject("con:KEY");
            String transId =(String) jsonObject1.get(PayloadXmlTransId).toString();
            String msisdn = (String) jsonObject1.get(PayloadXmlMsisdn).toString();
            String serviceName = (String) jsonObject2.get(PayloadXmlServiceName);

            System.out.println("transId = "+transId);

            String rvsMkr = service.rvsMkr(data);
            if(rvsMkr == null){
                SyncEligibilityResponse response =  responseError("INELIGIBLE", "81009998", "Maaf sedang terjadi kesalahan teknis, silahkan coba kembali beberapa saat lagi", transId, serviceName, msisdn);
                return new ResponseEntity<SyncEligibilityResponse>(response, HttpStatus.OK);
            }
//            else {
//                JSONObject rvsMkrJson = XML.toJSONObject(rvsMkr);
//                JSONObject getRvsMkrJson = rvsMkrJson.getJSONObject(responseHeader);
//                int statusRvs = (int) getRvsMkrJson.get(responseStatus);
//                System.out.println("response status = " + statusRvs);
//                if (statusRvs != 0) {
//                    SyncEligibilityResponse response =  responseError("INELIGIBLE", "1024", "Registration Failed - Not eligible Default", transId, serviceName, msisdn);
//                    return new ResponseEntity<SyncEligibilityResponse>(response, HttpStatus.OK);
//                }
//            }
            log.info(rvsMkr);

            int i = 0;
            boolean flag = false;
            String eligibilitySyncStatus = null;
            while (i < counter && !flag){
                try{
                    Thread.sleep(sleepTime);
                    System.out.println(i);
                    i++;
                    //query
                    try{
                        eligibilitySyncStatus = repo.getResponsePayload(transId);
                        if(eligibilitySyncStatus != null) {
                            flag = true;
                            repo.deleteById(transId);
                        }
                    }catch (Exception e){
                        SyncEligibilityResponse response =  responseError("INELIGIBLE", "81009998", "Maaf sedang terjadi kesalahan teknis, silahkan coba kembali beberapa saat lagi", transId, serviceName, msisdn);
                        return new ResponseEntity<SyncEligibilityResponse>(response, HttpStatus.OK);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                    throw new CustomException(e.getMessage(), HttpStatus.OK);
                }
            }
            if(flag == false){
                return new ResponseEntity<>("Empty data in table", HttpStatus.OK);
            }else {
                return new ResponseEntity<>(eligibilitySyncStatus, HttpStatus.OK);
            }

        }
        catch (Exception e){
            log.error(e.getMessage());
            return new ResponseEntity<>(e.getMessage(), HttpStatus.OK);
        }
    }

    private SyncEligibilityResponse responseError(String ineligible, String errorCode, String errorDesc, String transId, String serviceName, String msisdn) {
        ServiceDataResponse serviceDataResponse = new ServiceDataResponse();
        serviceDataResponse.setEligibility(ineligible);
        serviceDataResponse.setErrorcode(errorCode);
        serviceDataResponse.setErrordesc(errorDesc);
        serviceDataResponse.setExpirydate("");
        serviceDataResponse.setIsrebuy("");
        serviceDataResponse.setPackagename("");
        serviceDataResponse.setRebuytype("");
        serviceDataResponse.setIneligiblereason("");

        SyncEligibilityResponse response = new SyncEligibilityResponse(
                "Success",
                transId,
                serviceDataResponse,
                serviceName,
                "0",
                msisdn,
                transId
        );
        return response;
    }


    private static String convertDocumentToString(Document doc) {
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer transformer;
        try {
            transformer = tf.newTransformer();
            // below code to remove XML declaration
            // transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            StringWriter writer = new StringWriter();
            transformer.transform(new DOMSource(doc), new StreamResult(writer));
            String output = writer.getBuffer().toString();
            return output;
        } catch (TransformerException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static Document convertStringToXml(String xmlString) {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        try {
            dbf.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
            DocumentBuilder builder = dbf.newDocumentBuilder();
            Document doc = builder.parse(new InputSource(new StringReader(xmlString)));
            return doc;
        } catch (ParserConfigurationException | IOException | SAXException e) {
            throw new RuntimeException(e);
        }
    }
}
