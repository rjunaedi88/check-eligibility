package com.indosat.service;

import com.indosat.model.EligibilitySyncStatus;
import com.indosat.repository.EligibilitySyncStatusRepo;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;

@Service
public class CheckEligibilityService {

    @Value("${endpoint.rvs.mkr}")
    private String rvsMkrUrl;

    @Autowired
    EligibilitySyncStatusRepo repo;

    @Autowired
    private RestTemplate restTemplate;


    public boolean insertData (JSONObject param, String transId) throws Exception{
        boolean result = false;
        try {
            EligibilitySyncStatus eligibilitySyncStatus = new EligibilitySyncStatus();
            eligibilitySyncStatus.setCreatedTimestamp(new Timestamp(System.currentTimeMillis()));
            eligibilitySyncStatus.setResponsePayload(param.toString());
            eligibilitySyncStatus.setTrxId(transId);
            repo.save(eligibilitySyncStatus);
            result = true;
        }catch (Exception e){
            e.getStackTrace();
            throw new Exception(e.getMessage());
        }
        return result;
    }

    public String rvsMkr(String request){
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.TEXT_PLAIN);
            HttpEntity<String> req = new HttpEntity<>(request, headers);
            ResponseEntity<String> result = restTemplate.postForEntity(rvsMkrUrl, req, String.class);
            return result.getBody();
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }
}
