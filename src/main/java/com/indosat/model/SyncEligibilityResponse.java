package com.indosat.model;

import com.indosat.model.ServiceDataResponse;

public class SyncEligibilityResponse {

    private String servicedesc;
    private String transid;
    private ServiceDataResponse servicedata;
    private String servicename;
    private String servicestatus;
    private String msisdn;
    private String servicebetransid;

    public SyncEligibilityResponse(String servicedesc, String transid, ServiceDataResponse servicedata, String servicename, String servicestatus, String msisdn, String servicebetransid) {
        this.servicedesc = servicedesc;
        this.transid = transid;
        this.servicedata = servicedata;
        this.servicename = servicename;
        this.servicestatus = servicestatus;
        this.msisdn = msisdn;
        this.servicebetransid = servicebetransid;
    }

    public SyncEligibilityResponse() {

    }

    public String getServicedesc() {
        return servicedesc;
    }

    public void setServicedesc(String servicedesc) {
        this.servicedesc = servicedesc;
    }

    public String getTransid() {
        return transid;
    }

    public void setTransid(String transid) {
        this.transid = transid;
    }

    public ServiceDataResponse getServicedata() {
        return servicedata;
    }

    public void setServicedata(ServiceDataResponse servicedata) {
        this.servicedata = servicedata;
    }

    public String getServicename() {
        return servicename;
    }

    public void setServicename(String servicename) {
        this.servicename = servicename;
    }

    public String getServicestatus() {
        return servicestatus;
    }

    public void setServicestatus(String servicestatus) {
        this.servicestatus = servicestatus;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getServicebetransid() {
        return servicebetransid;
    }

    public void setServicebetransid(String servicebetransid) {
        this.servicebetransid = servicebetransid;
    }
}
