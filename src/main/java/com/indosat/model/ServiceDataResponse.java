package com.indosat.model;


public class ServiceDataResponse {
    private String expirydate;
    private String errordesc;
    private String rebuytype;
    private String isrebuy;
    private String packagename;
    private String eligibility;
    private String errorcode;
    private String ineligiblereason;

    public ServiceDataResponse() {

    }

    public ServiceDataResponse(String expirydate, String errordesc, String rebuytype, String isrebuy, String packagename, String eligibility, String errorcode, String ineligiblereason) {
        this.expirydate = expirydate;
        this.errordesc = errordesc;
        this.rebuytype = rebuytype;
        this.isrebuy = isrebuy;
        this.packagename = packagename;
        this.eligibility = eligibility;
        this.errorcode = errorcode;
        this.ineligiblereason = ineligiblereason;
    }

    public String getExpirydate() {
        return expirydate;
    }

    public void setExpirydate(String expirydate) {
        this.expirydate = expirydate;
    }

    public String getErrordesc() {
        return errordesc;
    }

    public void setErrordesc(String errordesc) {
        this.errordesc = errordesc;
    }

    public String getRebuytype() {
        return rebuytype;
    }

    public void setRebuytype(String rebuytype) {
        this.rebuytype = rebuytype;
    }

    public String getIsrebuy() {
        return isrebuy;
    }

    public void setIsrebuy(String isrebuy) {
        this.isrebuy = isrebuy;
    }

    public String getPackagename() {
        return packagename;
    }

    public void setPackagename(String packagename) {
        this.packagename = packagename;
    }

    public String getEligibility() {
        return eligibility;
    }

    public void setEligibility(String eligibility) {
        this.eligibility = eligibility;
    }

    public String getErrorcode() {
        return errorcode;
    }

    public void setErrorcode(String errorcode) {
        this.errorcode = errorcode;
    }

    public String getIneligiblereason() {
        return ineligiblereason;
    }

    public void setIneligiblereason(String ineligiblereason) {
        this.ineligiblereason = ineligiblereason;
    }
}
