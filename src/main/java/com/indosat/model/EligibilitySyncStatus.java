package com.indosat.model;

import javax.persistence.*;
import java.sql.Clob;
import java.sql.Date;
import java.sql.Timestamp;

@Entity
@Table(name = "eligibility_sync_status")
public class EligibilitySyncStatus {

    @Id
    @Column(name = "trx_id")
    private String trxId;

    @Column(name = "response_payload")
    @Lob
    private String responsePayload;

    @Column(name = "created_timestamp")
    private Timestamp createdTimestamp;

    public EligibilitySyncStatus() {
    }

    public EligibilitySyncStatus(String trxId, String responsePayload, Timestamp createdTimestamp) {
        this.trxId = trxId;
        this.responsePayload = responsePayload;
        this.createdTimestamp = createdTimestamp;
    }

    public String getTrxId() {
        return trxId;
    }

    public void setTrxId(String trxId) {
        this.trxId = trxId;
    }

    public String getResponsePayload() {
        return responsePayload;
    }

    public void setResponsePayload(String responsePayload) {
        this.responsePayload = responsePayload;
    }

    public Timestamp getCreatedTimestamp() {
        return createdTimestamp;
    }

    public void setCreatedTimestamp(Timestamp createdTimestamp) {
        this.createdTimestamp = createdTimestamp;
    }
}
