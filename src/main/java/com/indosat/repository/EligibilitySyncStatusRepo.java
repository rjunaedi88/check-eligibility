package com.indosat.repository;

import com.indosat.model.EligibilitySyncStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Clob;
import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;

@Repository
public interface EligibilitySyncStatusRepo extends JpaRepository<EligibilitySyncStatus, String> {
    List<EligibilitySyncStatus> findByTrxId(String transId);

    EligibilitySyncStatus findAllByTrxId(String transId);

    @Query("select a.responsePayload from EligibilitySyncStatus a where a.trxId=:trxId")
    String getResponsePayload(@Param("trxId") String trxId);


//    @Transactional
//    @Modifying
//    @Query("delete from EligibilitySyncStatus a where a.trxId=:trxId and a.createdTimestamp=:createdTimestamp")
//    void deleteRecord(@Param("trxId") String trxId, @Param("createdTimestamp") Timestamp createdTimestamp);
}
