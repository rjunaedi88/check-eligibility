package com.indosat.exception;

import org.springframework.http.HttpStatus;

public class CustomException extends Exception{

    private HttpStatus httpStatus;
    public CustomException(String message, HttpStatus httpStatus){
        super(message);
        this.httpStatus =httpStatus;
    }
}
